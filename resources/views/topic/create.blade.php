@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header bg-dark text-white">Create topic</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('topic.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label text-md-right">Title</label>
                                <div class="col-md-7">
                                    <input type="text" name="title" class="form-control" id="name"
                                           placeholder="Enter title">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label text-md-right">Description</label>
                                <div class="col-md-7">
                                    <input type="text" name="description" class="form-control" id="description"
                                           placeholder="Enter description">
                                </div>
                            </div>

                            <div class="form-group row">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection