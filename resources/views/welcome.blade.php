@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark text-white" style="text-align: center"><b>Topics</b></div>
                    @guest
                        <div class="card-body bg-info" style="text-align: center">
                            You have to be logged in to see the topics.
                        </div>
                    @else
                        <div class="card-body bg-info" style="text-align: center">
                            Here you can find all the topics.
                        </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table table-bordered" style="margin-top: 3%">
                    <thead>
                    <tr>
                        <th style="width: 15%;">Title</th>
                        <th style="width: 70%">Description</th>
                        <th style="width: 15%;">Created at</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($topics as $topic)
                            <tr>
                                <td><a href="">{{ $topic->title }}</a></td>
                                <td>{{ $topic->description }}</td>
                                <td>{{ $topic->created_at->format(' j F Y h:i A') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endguest
@endsection