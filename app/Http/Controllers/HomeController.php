<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();
        return view('welcome', compact('topics'));
    }
}
